package model.data_structures;

public class Node <T> {
	
	/**
	 * Nodo anterior del actual
	 */
	private Node<T> prior;
	
	/**
	 * Nodo siguiente al actual
	 */
	private Node<T> next;
	
	/**
	 * Constructor de la clase
	 */
	public Node(){
		
		prior = null;
		next = null;
	}
	
	/**
	 * Definir el nodo anterior
	 */
	public void setPrior(Node<T> n){
		
		prior = n;
	}
	
	/**
	 * Definir el nodo siguiente
	 */
	public void setNext(Node<T> n){
		
		next = n;
	}
	
	/**
	 * Retorna el nodo anterior
	 */
	public Node<T> getPrior(){
		return prior;
	}
	
	/**
	 * Retorna el nodo siguiente
	 */
	public Node<T> getNext(){
		return next;
	}
	
	public void disconnect(){
		prior.setNext(next);
		next.setPrior(prior);

	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
