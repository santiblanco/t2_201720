package model.data_structures;


/**
 * Abstract Data Type for a list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface IList<T> extends Iterable<T> {

	Integer getSize();
	
	public void addNodeK(Node<T> add, Node<T> next);
	
	public void addNodeBeginning(Node<T> add);
	
	public void addNodeEnd(Node<T> add);
	
	public Node<T> getElement(int i);
	
	public void deleteAtK(Node<T>N);

	

	
	
	
	

}
