package model.data_structures;

import java.util.Iterator;

public class DoublyLinkedList<T> implements IList<T> {

	//Atributos

	/**
	 * Primer nodo de la lista
	 */

	private Node<T> first;


	/**
	 * Ultimo nodo de la lista
	 */

	private Node<T> last;


	//Constructor

	public DoublyLinkedList(){
		first = null;
		last = null;
	}


	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public Integer getSize() {
		// TODO Auto-generated method stub

		int size = 0;
		if(first != null){
			Node<T> actual = first;
			while(actual.getNext()!=null){
				size++;
				actual = actual.getNext();
			}
		}
		return size;
	}


	@Override
	public void addNodeK(Node<T> add, Node<T> next) {
		// TODO Auto-generated method stub
		if(first == null){
			addNodeBeginning(add);
		}
		else{
			Node<T> actual = first;
			boolean found = false;
			while(actual.getNext()!= null && found==false){

				if(actual.getNext().equals(next)){
					add.setNext(next);
					actual.setNext(add);
					next.setPrior(add);
					add.setPrior(actual);
					found = true;
				}
				actual = actual.getNext();
			}
		}

	}


	@Override
	public void addNodeBeginning(Node<T> add) {
		// TODO Auto-generated method stub
		if(first == null){
			first = add;
			last = add;
		}
		else{
			add.setNext(first);
			first.setPrior(add);
			first = add;
		}

	}


	@Override
	public void addNodeEnd(Node<T> add) {
		// TODO Auto-generated method stub
		if(first == null){
			addNodeBeginning(add);
		}
		else{
			last.setNext(add);
			add.setPrior(last);
			last = add;
		}

	}



	public Node<T> getElement(int i) {
		// TODO Auto-generated method stub
		Node<T> elem = null;
		Node<T> actual = first;
		int index = 0;
		if(i == 0){
			elem = first;
		}
		else{

			while(actual.getNext()!= null && index != i){
				if(index == i){
					elem = actual;
				}
				actual = actual.getNext();
				index++;
			}
		}
		return elem;
	}


	@Override
	public void deleteAtK(Node<T> N) {
		// TODO Auto-generated method stub
		if(first != null){
			if(first.equals(N)){
				if(first.equals(last)){
					first = null;
					last = null;
				}
				else{
					first = first.getNext();
					first.setPrior(null);
				}

			}
			else if(last.equals(N)){
				last = last.getPrior();
				last.setNext(null);
			}
			else{
				N.disconnect();
			}
		}

	}



















}
