package model.data_structures;

import java.util.Iterator;

public class RingList<T> implements IList<T> {

	//Atributos

	/**
	 * Primer nodo de la lista
	 */

	private Node<T> first;


	/**
	 * Ultimo nodo de la lista
	 */

	private Node<T> last;


	//Constructor

	public RingList(){
		first = null;
		last = null;
	}


	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public Integer getSize() {
		// TODO Auto-generated method stub

		int size = 0;
		if(first != null){
			if(first.equals(last)){
				size = 1;
			}
			else{
				Node<T> actual = first;
				while(actual.getNext()!= first){
					size++;
					actual = actual.getNext();
				}
			}
		}

		return size;
	}


	@Override
	public void addNodeK(Node<T> add, Node<T> next) {
		// TODO Auto-generated method stub
		if(first == null){
			addNodeBeginning(add);
		}
		else{
			Node<T> actual = first;
			boolean found = false;
			while(actual.getNext()!= first && found==false){

				if(actual.getNext().equals(next)){
					add.setNext(next);
					actual.setNext(add);
					next.setPrior(add);
					add.setPrior(actual);
					found = true;
				}
				actual = actual.getNext();
			}
		}

	}


	@Override
	public void addNodeBeginning(Node<T> add) {
		// TODO Auto-generated method stub
		if(first == null){
			first = add;
			last = add;
			first.setPrior(last);
			last.setNext(first);
		}
		else{
			add.setNext(first);
			first.setPrior(add);
			first = add;
			first.setPrior(last);
			last.setNext(first);
		}

	}


	@Override
	public void addNodeEnd(Node<T> add) {
		// TODO Auto-generated method stub
		if(first == null){
			addNodeBeginning(add);
		}
		else{
			last.setNext(add);
			add.setPrior(last);
			last = add;
			last.setNext(first);
			first.setPrior(last);
		}

	}



	public Node<T> getElement(int i) {
		// TODO Auto-generated method stub
		Node<T> elem = null;
		Node<T> actual = first;
		int index = 0;
		if(i == 0){
			elem = first;
		}
		else{

			while(actual.getNext()!= first && index != i){
				if(index == i){
					elem = actual;
				}
				actual = actual.getNext();
				index++;
			}
		}
		return elem;
	}


	@Override
	public void deleteAtK(Node<T> N) {
		// TODO Auto-generated method stub
		if(first != null){
			if(first.equals(N)){
				if(first.equals(last)){
					first = null;
					last = null;
				}
				else{
					first = first.getNext();
					first.setPrior(last);
					last.setNext(first);
					
				}

			}
			else if(last.equals(N)){
				last = last.getPrior();
				last.setNext(first);
				first.setPrior(last);
			}
			else{
				N.disconnect();
			}
		}
	}
}
