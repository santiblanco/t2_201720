package esqueleto_t2_201720.test;

import model.data_structures.DoublyLinkedList;
import model.data_structures.Node;
import junit.framework.TestCase;

public class DoublyLinkedListTest extends TestCase {
	
	
	//Atributos
	
	/**
	 * Clase donde se haran las pruebas
	 */
	private DoublyLinkedList<Integer> DList;
	
	private Node<Integer> nodeT;	
	/**
	 * Construye una lista vac�a
	 */
	
	private void setupEscenario1(){
		
		try{
			DList = new DoublyLinkedList<Integer>();
		}
		catch(Exception e){
			fail ("No debio generar error al crear una lista nueva");
		}
	}
	
	private void setupEscenario2(){
		
		try{
			DList = new DoublyLinkedList<Integer>();
			Node<Integer> node1 = new Node<Integer>();
			DList.addNodeBeginning(node1);
			Node<Integer> node2 = new Node<Integer>();
			DList.addNodeBeginning(node2);
			Node<Integer> node3 = new Node<Integer>();
			DList.addNodeBeginning(node3);
			nodeT = new Node<Integer>();
			DList.addNodeBeginning(nodeT);
		}
		catch(Exception e){
			fail ("No debio generar error al crear una lista nueva");
		}
	}
	
	public void testDoublyLinkedList(){
		setupEscenario1();
		assertEquals("La lista no debe tener ningun elemento",null,DList.getElement(0));
	}
	public void testGetSize(){
		setupEscenario1();
		Node<Integer> node = new Node<Integer>();
		DList.addNodeBeginning(node);
		assertEquals("Deberia haber un nodo en la lista",new Integer(1), DList.getSize());
		
	}
	public void testAddNodeK(){
		setupEscenario2();
		Node<Integer> nodeB = new Node<Integer>();
		DList.addNodeK(nodeB, nodeT);
		assertEquals("El nodo siguiente debe ser nodeT",nodeT,nodeB.getNext());
		assertEquals("El nodo anterior debe ser null",null,nodeB.getPrior());	
		
	}
	public void testAddNodeBeginning(){
		setupEscenario2();
		Node<Integer> nodeBeg = new Node<Integer>();
		DList.addNodeBeginning(nodeBeg);
		assertEquals("El nodo siguiente debe ser nodoT",nodeT, nodeBeg.getNext());
		assertEquals("El nodo anterior debe ser null",null, nodeBeg.getPrior());
		
	}
	
	public void testAddNodeEnd(){
		setupEscenario2();
		Node<Integer> nodeEnd = new Node<Integer>();
		DList.addNodeEnd(nodeEnd);
		assertEquals("El nodo siguiente debe ser null",null, nodeEnd.getNext());
		assertEquals("El nodo anterior debe ser null",null, nodeBeg.getPrior());
		
	}
	
	
	
	
	
	
	
	
	
	

}
